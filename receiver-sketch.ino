/* ============================================
This code (and the hole project) is shared under the CC-BY-SA license -> https://creativecommons.org/licenses/by-sa/4.0/
As all this work is based on a lot of others work sharing their knowledge the same way I want to do the same.
Hopefully it will contribute to making "controlling Ableton LIve with Arduino" a little more easier.

More about who I am: http://hypponix.fr
And here is the gitlab wiki of this project: https://gitlab.com/FabLabEscape/arduino-glove/-/wikis/Flexsensor

I hope you'll enjoy
===============================================
*/

#include <AltSoftSerial.h>
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();

AltSoftSerial altSerial;
byte i = 0;

// set ASCII number to their letters
const byte y = 121; // for Yam values
const byte p = 112; // for Pitch values
const byte r = 114; // for Roll values
const byte f = 102; // for flexSensor values

// will allow us to recreate integers from chars and use it
int yamValue;
int pitchValue;
int rollValue;
int flexValue;


// variables to manipulate data from HM-10
char charArray[] = {NULL, NULL, NULL, NULL};
int value;

// set a min value for the flexSensor over which midi notes will be triggered
int threshold = 600;
int off = 300;

// will allow us to send only one note when closing or opening our hand
bool setNoteOn = true;
bool setNoteOff = true;

// define midi note that will be played (48 = C2)
int noteArray[] = {48, 55, 51, 48};
byte noteNbr = 0;

void setup() {
  MIDI.begin();
  Serial.begin(115200);
  Serial.println("AltSoftSerial Test Begin");
  altSerial.begin(9600);
  altSerial.println("Hello World");
}

void resetCharArray() {
  i = 0;
  charArray[0] = NULL;
  charArray[1] = NULL;
  charArray[2] = NULL;
  charArray[3] = NULL;
}

void loop() {
  char c;
    
  if (altSerial.available()) {
    c = altSerial.read();
    //Serial.print( c, DEC ); //helps to check witch ASCII values are actually sent through the HM-10
    switch (c) {
      case f:
        value = atoi(charArray);
        flexValue = value;
        resetCharArray();
        break;
      case y:
        value = atoi(charArray);
        yamValue = value;
        resetCharArray();
        break;
      case p:
        value = atoi(charArray);
        pitchValue = value;
        resetCharArray();
        break;
      case r:
        value = atoi(charArray);
        rollValue = value;
        resetCharArray();
        break;
      default:
        charArray[i] = c;
        i = i + 1;
    }
/*
// See the received values in monitor
    Serial.print("\t yam: ");
    Serial.print(yamValue);
    Serial.print("\t pitch: ");
    Serial.print(pitchValue);
    Serial.print("\t roll: ");
    Serial.print(rollValue);
    Serial.print("\t flex: ");
    Serial.println(flexValue);
*/
// Uncomment the paragraphe if you want to send MIDI note
    if (flexValue > 700) {
      int setYamToMidi = map(yamValue, -45, 45, 0, 127);
      MIDI.sendControlChange(0, setYamToMidi, 1);
    } else if (flexValue < 700 && flexValue > 600) {
      int setPitchToMidi = map(pitchValue, -45, 45, 0, 127);
      MIDI.sendControlChange(1, setPitchToMidi, 1);
    } else {
      int setRollToMidi = map(rollValue, -45, 45, 0, 127);
      MIDI.sendControlChange(2, setRollToMidi, 1);
    }
  }
}