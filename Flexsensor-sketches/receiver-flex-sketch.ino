/* ============================================
This code (and the hole project) is shared under the CC-BY-SA license -> https://creativecommons.org/licenses/by-sa/4.0/
As all this work is based on a lot of others work sharing their knowledge the same way I want to do the same.
Hopefully it will contribute to making "controlling Ableton LIve with Arduino" a little more easier.

More about who I am: http://hypponix.fr
And here is the gitlab wiki of this project: https://gitlab.com/FabLabEscape/arduino-glove/-/wikis/Flexsensor

I hope you'll enjoy
===============================================
*/

#include <AltSoftSerial.h>
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();

AltSoftSerial altSerial;
byte i = 0;

// set ASCII number to their letters because we're sending the letter from the sender-sketch
const byte f = 102; // f for flexSensor values

// will allow us to recreate integers from chars and use it
int flexValue;

// variables to manipulate data from HM-10
char charArray[] = {0, 0, 0, 0};
int value;

// set a min value for the flexSensor over which midi notes will be triggered
int threshold = 600;

// will allow us to send only one note when closing or opening our hand
bool setNoteOn = true;
bool setNoteOff = true;

// define midi note that will be played (48 = C2)
int noteArray[] = {48, 55, 51, 48};
byte noteNbr = 0;

void setup() {
  MIDI.begin();
  Serial.begin(9600);
  Serial.println("AltSoftSerial Test Begin");
  altSerial.begin(9600);
  altSerial.println("Hello World");
}

void resetCharArray() {
  i = 0;
  charArray[0] = 0;
  charArray[1] = 0;
  charArray[2] = 0;
  charArray[3] = 0;
}

void loop() {
  char c;
    
  if (altSerial.available()) {
    c = altSerial.read();
    switch (c) {
      case f:
        value = atoi(charArray);
        flexValue = value;
        resetCharArray();
        break;
      default:
        charArray[i] = c;
        i = i + 1;
    }

// See the received values in monitor
    Serial.print("\t flex: ");
    Serial.println(flexValue);
    
/*// Uncomment the paragraphe if you want to send MIDI note
    if (flexValue > threshold) {
      if (setNoteOn == true) {
        MIDI.sendNoteOn(noteArray[noteNbr], 127, 1);
        setNoteOn = false;
        setNoteOff = true;
      }
    } else {
      if (setNoteOff == true) {
        MIDI.sendNoteOff(noteArray[noteNbr], 127, 1);
        setNoteOff = false;
        setNoteOn = true;
        if (noteNbr < 3) {
          noteNbr = noteNbr + 1;
        } else {
          noteNbr = 0;
        }
      }
    }*/
  }
}