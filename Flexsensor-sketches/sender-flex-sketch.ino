/* ============================================
This code (and the hole project) is shared under the CC-BY-SA license -> https://creativecommons.org/licenses/by-sa/4.0/
As all this work is based on a lot of others work sharing their knowledge the same way I want to do the same.
Hopefully it will contribute to making "controlling Ableton LIve with Arduino" a little more easier.

More about who I am: http://hypponix.fr
And here is the gitlab wiki of this project: https://gitlab.com/FabLabEscape/arduino-glove/-/wikis/Flexsensor

I hope you'll enjoy
===============================================
*/

// set AltSoft library to make it easy to send and receive bluetooth info
#include <AltSoftSerial.h>
AltSoftSerial altSerial;

// flexsensor initialisation
const int flexPin = A0;
int flexValue;

void setup() {
    // start communication through AltSoft lib
    Serial.begin(9600);
    Serial.println("AltSoftSerial Test Begin");
    altSerial.begin(9600);
    altSerial.println("Hello World");
}

void loop() {
    
    flexValue = analogRead(flexPin);
    Serial.print("\t");
    Serial.println(flexValue);
    altSerial.println(flexValue);
    altSerial.print("f");
    
    delay(50);
}