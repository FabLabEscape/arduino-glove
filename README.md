# arduino-glove

**environment: macOS High Sierra 10.13.6**

* Starting with [this tutorial](https://www.instructables.com/id/Send-and-Receive-MIDI-with-Arduino/). No need to use the midi circuit if sending MIDI through Arduino USB cable.
* Then download [Hairless midi](http://projectgus.github.io/hairless-midiserial/).
* If needed, turn on the [IAC Midi driver](https://www.youtube.com/watch?v=hgFA_fdup7g).
* Then [learn how to get signal into ableton](https://www.youtube.com/watch?v=AepicXO7Dlc) using Hairless midi.
* While proceeding, download [Arduino Midi Library](https://playground.arduino.cc/Main/MIDILibrary/) for the software part. Then go the the library documentation to get started.
* Make sure [Ableton is well configure](https://help.ableton.com/hc/en-us/articles/209774205-Live-s-MIDI-Ports-explained) to receive the signal.
* To go further [here is the documentation](http://fortyseveneffects.github.io/arduino_midi_library/) for the Arduino MIDI Library.
* And [MIDI specs documentation](http://www.somascape.org/midi/tech/spec.html#rpns) to add all possibilities in your code.

# To go further

As a first try, I've used a flex sensor and map the values to get MIDI notes in Ableton. Here is an instructable tutorial to get started with [flex sensor](https://www.instructables.com/id/How-to-use-a-Flex-Sensor-Arduino-Tutorial/).
* Also check out [Notes and Volts](https://www.youtube.com/playlist?list=PL4_gPbvyebyH2xfPXePHtx8gK5zPBrVkg) Youtube channel dedicated to arduino and midi controller
* Here a great [tutorial from arduino.cc](https://www.arduino.cc/en/Tutorial/MidiDevice) to learn how to built a midi controller

## To connect bluetooth to Arduino
* Follow [this tutorial](https://arduino.blaisepascal.fr/bluetooth/). You can also watch this [Youtube video](https://www.youtube.com/watch?v=E-1w7dL3Cps) of  How To Mechatronics.
* I used the HM-10 (actually HM-10S-A) bluetooth module and had very hard trouble to configure it in Slave mode because no AT command was working. If it is the case for you:
    * Change from Mac to Windows.
    * Read this [amazing article](http://nerdclub-uk.blogspot.com/2016/02/working-with-cheap-bluetooth-btle4.html).
    * Download the app they're talking about : it is actually called [PC ComAssistant](http://www.jnhuamao.cn/index_en.asp?ID=1).
    * Connect your bluetooth module to the comuter using an FTDI cable [I used a TTL-232R-5V](https://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf) (don't forget to invert Tx & Rx) and select the right COM port in the app.
    * It should work this way. [Here](http://fab.cba.mit.edu/classes/863.15/doc/tutorials/programming/bluetooth.html) or [There](https://www.teachmemicro.com/hc-05-bluetooth-command-list/) are few AT commands you can use.
* Go through [Martyn Currey's](http://www.martyncurrey.com/hm-10-bluetooth-4ble-modules/) domentation if needed.

## To connect 2 HM-10 together
* Check [Martyn Currey's](http://www.martyncurrey.com/hm-10-bluetooth-4ble-modules/) tutorial
* My HM-10s where fake and I had hard time making them work together. In the end Martyn Currey's tutorial worked really well (thanks again for the doc btw) but depending on the use you want to have of HM-10 using the auto-connect mode make it easier way (note that I also had trouble talking to HM-10 through the Arduino to I configured it so I only use the FTDI cable). For that:
    * Change from Mac to Windows.
    * Read this [amazing article](http://nerdclub-uk.blogspot.com/2016/02/working-with-cheap-bluetooth-btle4.html).
    * Download the app they're talking about : it is actually called [PC ComAssistant](http://www.jnhuamao.cn/index_en.asp?ID=1).
    * Connect your bluetooth module to the comuter using an FTDI cable [I used a TTL-232R-5V](https://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf) (don't forget to invert Tx & Rx) and select the right COM port in the app.
    * [Here](http://fab.cba.mit.edu/classes/863.15/doc/tutorials/programming/bluetooth.html) or [There](https://www.teachmemicro.com/hc-05-bluetooth-command-list/) are few AT commands you can use.
    * You should use AT+CON[address] and AT+ROLE[param] to pair to another device. For exemple :
        * HM-10 n°1:
            * AT+ADDR? to get the address, you should get something like "0CB2B77BCB21". Write it down.
            * AT+ROLE0 to set this HM-10 as a Slave / Periferal device.
            * AT+RESET you might have to reset the device so it takes into account the role
        * HM-10 n°2:
            * AT+ADDR? to get the address, let say you get that for exemple "341315FE0DDA"
            * AT+ROLE1 to set this HM-10 as a Master / Central device.
            * AT+RESET for the same reason
            * AT+CON0CB2B77BCB21 so this HM-10 will connect with the other one.
        * HM-10 n°1 again:
            * AT+CON341315FE0DDA so HM-10 n°1 will also look for HM-10 n°2 and connect together.
    * Of course both addresses needs to be the ones from each of your HM-10. 

# Use Arduino Pro Mini
## Uploading a sketch on the board

* Connect the Arduino Pro Mini with an FTDI cable (I used a TTL-232R-5V and you can find the [datasheet here](https://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_CABLES.pdf)) as followed :
    * 1 (GND, black wire) to the GND
    * 2 (CTS, brown wire) to BLK
    * 3 (VCC, red wire) to VCC
    * 4 (TXD, orange wire) to Rx
    * 5 (RXD, yellow wire) to Tx
    * 6 (RTS, green wire) to GRN
    * *If you don't use CTS & RTS you can still upload by holding the reset button on the Arduino board while launching the upload. As soon as the upload starts you should release the button for the upload to start.*
* Upload a sketch by selecting first **Tool** > **Board** menu > **Arduino Nano** (and not Arduino Mini or Pro Mini or anything else [as it it said](https://www.arduino.cc/en/Guide/ArduinoProMini). Or at least only one way worked for me)
* The in the **Tool** section again select your microcontroller (for me it is ATmega328P)
* Then upload your sketch. You can use the one from [Martyn Currey's tutorial](http://www.martyncurrey.com/hm-10-bluetooth-4ble-modules/#arduinoToHM-10). I've choose to use the one from the [AltSoftSerial Library](https://www.pjrc.com/teensy/td_libs_AltSoftSerial.html), library that you'll need to use anyway :)
* Of course, if you haven't added the AltSoftSerial library to your Arduino IDE, you should add it first. It is available from the Arduino IDE Library, under **Sketch** > **Include library** > **manage library**.

## Connect HM-10 to Arduino

Thank to the AltSoftSerial Library, the Tx / Rx pins on the Arduino are now D8 and D9. So you need to connect :
* D8 pin from Arduino to the Rx pin of the HM-10
* D9 pin from the Arduino to the Tx pin of the HM-10. /!\ you should connect the pins through a voltage divider as describe on the Martyn Currey's tutorial.
* VCC to VCC :)
* GND to GND :)

## Add flex sensor and send the data

On one arduino (the sender which should be connected to the master / central HM-10):
* You'll need to plug the flex sensor [like that](https://cdn.instructables.com/F3R/F7DY/IBW24SQP/F3RF7DYIBW24SQP.LARGE.jpg?auto=webp&width=1024&fit=bounds) without the led. If you want to know more about the flex sensor you can check the [hole instructable](https://www.instructables.com/id/How-to-use-a-Flex-Sensor-Arduino-Tutorial/) about it.
* Add [this sketch](https://gitlab.com/FabLabEscape/arduino-glove/-/blob/master/sender-sketch.ino). 

On the other Arduino (the receiver which should be connected to the slave / periferal HM-10):
* Add [this sketch](https://gitlab.com/FabLabEscape/arduino-glove/-/blob/master/receiver-sketch.ino)

***Good to know:** data are in ASCII and that's why you'll find y = 121 in the code for exemple. If you want to know more about translation, you can [check this table](https://www.asciitable.com/).*

## Use Arduino MIDI Library to send data via USB
* Get the library [here](https://playground.arduino.cc/Main/MIDILibrary/). You'll need it for the sketch above as well since the library is included.
* Add the previous ***receiver sketch*** to the arduino.
* Then download [Hairless midi](http://projectgus.github.io/hairless-midiserial/).
* If needed, turn on the [IAC Midi driver](https://www.youtube.com/watch?v=hgFA_fdup7g).
* Then [learn how to get signal into ableton](https://www.youtube.com/watch?v=AepicXO7Dlc) using Hairless midi.
* I've used Ableton and you need to make sure [it is well configured](https://help.ableton.com/hc/en-us/articles/209774205-Live-s-MIDI-Ports-explained) to receive the signal.
* To go further [here is the documentation](http://fortyseveneffects.github.io/arduino_midi_library/) for the Arduino MIDI Library.
* And here some [MIDI documentation](http://www.somascape.org/midi/tech/spec.html#rpns) to add all possibilities in your code.

And you should be good to go :)

# Use MPU6050 to get accelerometer values
* [This schema](https://www.instructables.com/id/Accelerometer-MPU-6050-Communication-With-AVR-MCU/) from Instructable is a good base.
* If you use an Arduino Pro Mini like me, remember that pin A4 and A5 are [here](https://projectiot123.com/wp-content/uploads/2019/04/Arduino-PRo-MINI-pinout.jpg).
* There is also usefull information from [arduino.cc](https://playground.arduino.cc/Main/MPU-6050), especially an [exemple of code](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/examples/MPU6050_raw/MPU6050_raw.ino).
* For that you'll need 3 libraries :
    * Wire.h, you can get it directly from the Arduino IDE Library.
    * [I2Cdev](https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/I2Cdev).
    * [MPU6050](https://github.com/jrowberg/i2cdevlib/tree/master/Arduino/MPU6050).
* I've based my code on Jeff Rowberg's examples and [this one](https://github.com/jrowberg/i2cdevlib/blob/master/Arduino/MPU6050/examples/MPU6050_DMP6/MPU6050_DMP6.ino). will make it way easier to use Yam / Pitch / Roll value instead of raw values.
* And then here is [the doc](https://www.i2cdevlib.com/docs/html/class_m_p_u6050.html#a7c0146d45537e4bd7a0d4c1c476fdab7) if you want to go further.
* 